clc
h = 6.62607015e-034 %Planck constant has been sourced from https://physics.nist.gov/cgi-bin/cuu/Value?h|search_for=universal_in! on 18.10.2019

fprintf('Ex1: h/(2*pi) = %e \n',h/(2*pi)) % %e expects a vaule in the exponential form
disp('Please press enter to continue...')
pause

fprintf('Ex2: sin(30*/e) = %f \n', sind(30/exp(1))) % sind() calculates the sine of the argument in degrees, exp() function returns the value of exponential funtion for the given parameter
disp('Please press enter to continue...')
pause

fprintf('Ex3: 0x00123d3/(2,455*10^5) = %f \n', hex2dec('123d3')/2.455/1e5) %hex2dec() function converts hexadecimal string number into a decimal number
disp('Please press enter to continue...')
pause

fprintf('Ex4: sqrt(e-pi) = %fi ', imag(sqrt(exp(1) - pi))) %imag() return the imaginary value of a complex number
disp('Please press enter to continue...')
pause

fprintf('Ex5: Pi"s 11th significant digit is %d \n ', mod(floor(pi*1e10),10)) %mod calculates the remainder, and floor() rounds down the number
disp('Please press enter to continue...')
pause

fprintf('Ex6: The number of days since my birthday which is on 19.08 is %d ', datenum(date) - datenum('19-Aug-2019')) %datenum returns the number of days that have passed since 01.01.1970, date return the current date in 'dd-mmm-yyyy' format
disp('Please press enter to continue...')
pause

Re = 6378.137 % Earth radius in km taken from https://nssdc.gsfc.nasa.gov/planetary/factsheet/earthfact.html
fprintf('Ex7: arctan((e^(sqrt(7)/2 - log(Re/10^5)))/(0xaabb)) = %f \n',atan((exp(sqrt(7)/2 - log(Re/1e5)))/(hex2dec('aabb'))))
disp('Please press enter to continue...')
pause

Na = 6.02214076e23 %Avogadro constant taken from https://physics.nist.gov/cgi-bin/cuu/Value?na|search_for=avogadro on 18.10.2019
fprintf('Ex8: The number of atoms in 0.2 micromols of ethanol is %e \n', 0.2*1e-6*Na)
disp('Please press enter to continue...')
pause

fprintf('Ex9: %f �', 2/9*1/101*1000 )
disp('Please press enter to end the script... \n')
pause





